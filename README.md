# README #

This repo is for clustering similar lines in a large corpus divided to sub-corpora.

### Process ###

* First round of clustering
    1. Splitting data into sub-corpora (by country IDs).
    2. Pre-processing, cleaning line, character normalization.
    3. Spell-checking using alphabetic types as vocab with gestalt patter matching (difflib).
    4. Split lines into character bigrams.
    5. Cretae tf-idf matrix from bigrams.
    6. Running clustering algorithm of choice (default: dbscan) on matrix.
    7. Write interim results to `post_input`.
    8. Type 1 reclustering (under-matching problem): split large clusters as described in 3-6.
    9. Write interim results to `post_output`.
* Second round of clustering
    10. Merge split clusters back to previous results to directory `before_reclustering`.
    11. Type 2 reclustering (over-matching problem): merge over-split cluster:
        + re-run the clustering algorithm (as in described in steps 3-6.) on first elements of each cluster
    12. Write results to `recluster_output`.
    13. Re-map new clusters to previous result and write to `final_output/0.nn`.
    14. Re-number clusters incrementing them by 1 at a time, and write results to `final_output/renumber_0nn`.

### Requirements ###

To create virtual environment, run:

    conda create -n <new_environment> --file requirements.txt

### How to run ###

    cd source
    version=`grep 'self.version' cluster_lines.py | grep -E -o '[0-9]+\.[0-9]+'`
    source activate <environment_name>
    python split_data.py
    python cluster_lines py
    python post.py ${version}
    python merge_usa.py
    ./prepare_recluster_input.sh
    python recluster_lines.py <eps>
    python renumber.py
    
### Warnings ###

+ wrapper script is not ready yet,
+ if input files not correctlt placed, the scripts probably fail,
+ processing country 249 requires extra steps not described in the readme
