#!/bin/bash

version=$1
for dir in `find -maxdepth 1 -type d -ls | sed 's/\.\///g' | grep -v ' \.' | tr -s ' ' |cut -d' ' -f11`
do
	mkdir -p ${dir}/${version}
	mv ${dir}/*.txt ${dir}/${version}
done
