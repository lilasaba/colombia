#!/bin/bash

INPUT=../input/249.txt
NR_LINES=`wc -l $INPUT | grep -o "^[0-9]\+"`
MAX_LINES_PER_FILE=100000
PARTS=`echo "if ( ${NR_LINES}%${MAX_LINES_PER_FILE} ) ${NR_LINES}/${MAX_LINES_PER_FILE}+1 else ${NR_LINES}/${MAX_LINES_PER_FILE}" | bc`
echo 'Split input file into ' $PARTS

split -n ${PARTS} --numeric-suffixes=1 -a 1 --additional-suffix=_usa.txt $INPUT
for fil in *usa.txt
do
	new_file=${fil/x/}
	mv $fil ../input/${new_file}
done
