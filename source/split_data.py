# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import re
from collections import OrderedDict

def write_out(code,inlist):
    with open('../input/%s.txt' % code,'w',encoding='utf-8') as outf:
        for line in inlist:
            outf.write('%s\n' % line)

cc = re.compile(r'(?<=,)[0-9]+\n?$')

## Open file containing official country codes.
country_codes = set()
with open('../cc_off',encoding='utf-8') as countries:
    for line in countries:
        line = line.rstrip()
        ## Handle 023--23 pairs.
        if line[0] == '0':
            twodigit = line[1:]
        else:
            twodigit = line
        country_codes.add(line)
        country_codes.add(twodigit)

country_dict = OrderedDict()
with open('../foreignfirmnames.csv',encoding='utf-8') as frgn:
    for line in frgn:
        line = line.rstrip()
        if cc.search(line):
            code = cc.search(line).group()
            if len(code) == 2:
                code = '0%s' % code
        else:
            print('Garbage line || %s' % line)
            continue
        if code in country_codes:
            try:
                country_dict[code].append(line)
            except KeyError:
                country_dict[code] = [line]

for c in list(country_codes):
    try:
        write_out(c,country_dict[c])
    except KeyError:
        print('Empty country code',c)
