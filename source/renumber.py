# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import glob
import os

def process(eps):
    for fn in glob.iglob('../final_output/0.%s/*.txt' % eps):
        outname = fn.replace('/0.%s/' % eps,'/renumber_0%s/' % eps)
        print('%s\t%s' % (fn,outname))
        cluster_number = 0
        with open(fn,encoding='utf-8') as inp, open(outname,'w',encoding='utf-8') as out:
            for line in inp:
                if 'Cluster [' in line:
                    out.write('Cluster [%s]\n' % cluster_number)
                    cluster_number += 1
                else:
                    out.write(line)

if __name__ == '__main__':
    eps_list = ['82','62']
    for eps in eps_list:
        try:
            os.mkdir('../final_output/renumber_0%s' % eps)
        except FileExistsError:
            pass
        process(eps)
