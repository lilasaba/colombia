BEFORE=../before_reclustering

ls ../post_output/*txt | grep -o '[0-9]\+\_output' > split_list

for fn in ../output/*txt
	do 
		if [[ ${fn} != *"Cluster"* ]]; then
			filename="${fn##*/}"
			country_code=${filename/_0.*/}
			if grep -F ${country_code} split_list; then
				echo 'do not copy' ${fn}
				echo 'CC' ${country_code}
			else
				cp $fn ${BEFORE}
			fi
		fi
	done

for sfn in ../post_output/*txt
	do
		if [[ ${sfn} != *"usa"* ]]; then
			echo 'copying' $sfn
			cp $sfn ${BEFORE}
		fi
	done

cd ../before_reclustering
rm *usa*

## Rename files.
for bfn in *txt
do
		out0=${bfn/_split_large_clusters/}
		out=${out0/.txt/_before_reclustering.txt}
		mv $bfn $out
done
