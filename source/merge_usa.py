# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import glob
from collections import OrderedDict
from cluster_lines import CLUSTER

dummy = CLUSTER('dummy',0.82)
version = dummy.version 

usad = OrderedDict()
nrclus = 0
for fn in glob.iglob('../post_output/*usa*.txt'):
    print(fn)
    print('Nr of clusters before',len(usad.keys()))
    with open(fn,encoding='utf-8') as inf:
        for line in inf:
            if 'Cluster' in line and line[0] == 'C':
                cl = 'Cluster [%d]\n' % nrclus
                nrclus += 1
                usad[cl] = []
            else:
                usad[cl].append(line)
    print('Nr of clusters after',len(usad.keys()))
print('Nr of all clusters in usad',len(usad.keys()))

with open('../post_output/249_output_0.62_%s_split_large_clusters.txt' % version,'w',encoding='utf-8') as outf:
    for clus in usad.keys():
        outf.write(clus)
        for ln in usad[clus]:
            outf.write(ln)
