# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import glob
import os
import sys

from cluster_lines import CLUSTER
from collections import OrderedDict
from time import time

def extract_clusters(fn):
    d_all = {}
    d_cluster = OrderedDict()
    d_first_line_of_cluster = []
    line_count = 0
    cluster_number = None
    if_first_line_of_cluster = False
    to_recluster = fn.replace('/before_reclustering/','/recluster_input/').replace('.txt','')
    to_recluster = '%s_to_recluster.txt' % to_recluster
    with open(fn,encoding='utf-8') as res, open(to_recluster,'w',encoding='utf-8') as out:
        for line in res:
            line = line.rstrip()
            line_count += 1
            if 'Cluster' in line and line[0] == 'C':
                cluster_number = int(line.split()[1].replace('[','').replace(']',''))
                if_first_line_of_cluster = True
                d_cluster[cluster_number] = []
            elif if_first_line_of_cluster:
                out.write('%s-%s\n' % (line,line_count))
                if_first_line_of_cluster = False
                d_all[line_count] = [line,cluster_number,line_count]
                d_cluster[cluster_number].append(line)
            else:
                d_all[line_count] = [line,cluster_number,line_count]
                d_cluster[cluster_number].append(line)

    print('Line count:',line_count)

    return d_all, d_cluster, to_recluster

def merge_clusters(d_cluster,cluster_nums):
    c = 0
    for cluster in cluster_nums:
        if c == 0:
            proto_cluster = cluster
        else:
            for line in d_cluster[cluster]:
                d_cluster[proto_cluster].append(line)
            d_cluster.pop(cluster)
        c += 1

    return d_cluster

def remap_new_clusters(reclustered_fn,d_all,d_cluster,eps):
    d_rec = {}
    merged = reclustered_fn.replace('recluster_output/','final_output/%s/' % eps,1).replace('.txt','_merged.txt')
    with open(reclustered_fn,encoding='utf-8') as rec:
        for line in rec:
            line = line.rstrip()
            if 'Cluster' in line:
                cl = line
                d_rec[cl] = []
            else:
                d_rec[cl].append(line)

    ## Find clusters consisting of 2+ elements.
    for cluster in d_rec.keys():
        if len(d_rec[cluster]) > 1:
            cluster_nums = []
            for line in d_rec[cluster]:
                line_number = int(line.split('-')[-1])
                try:
                    #print(d_all[line_number],line_number)
                    cl_num = d_all[line_number][1]
                    cluster_nums.append(cl_num)
                    d_cluster = merge_clusters(d_cluster,cluster_nums)
                except KeyError:
                    print('KeyError',line_number)
                    pass
            #print(20*'-')
        
    with open(merged,'w',encoding='utf-8') as out:
        print(merged)
        for clus in d_cluster.keys():
            out.write('Cluster [%s]\n' % clus)
            for line in d_cluster[clus]:
                out.write('%s\n' % line)
        pass

if __name__ == '__main__':
    eps = float(sys.argv[1])
    try:
        os.mkdir('../final_output/%s' % eps)
    except FileExistsError:
        pass
    ready = set()
    with open('../done',encoding='utf-8') as done:
        for line in done:
            line = line.rstrip()
            ready.add(line)
    for fn in glob.iglob('../before_reclustering/*txt'):
        country_code = os.path.basename(fn).split('_')[0]
        if country_code not in ready:
            d_all,d_cluster,to_recluster = extract_clusters(fn)
            dbscan = CLUSTER(to_recluster,eps) 
            dbscan.process()
            for rec in glob.glob('../recluster_output/%s_*to_recluster_output_%s_%s.txt' % (country_code,eps,dbscan.version)):
                if 'clean' not in rec:
                    reclustered_fn = rec
            remap_new_clusters(reclustered_fn,d_all,d_cluster,eps)
