#!bin/bash

prev_version=$1
declare -a dirs=("input" "final_output" "post_input" "recluster_output" "post_output" "before_reclustering" "output" "recluster_input")

for dir in "${dirs[@]}"
do
	cd ../${dir}
	mkdir -p ${prev_version}
	mv *.txt ${prev_version}
done
