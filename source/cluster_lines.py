# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# -*- encoding: utf-8 -*-

import functools
import re
import numpy as np
import random
import sys
import unicodedata

from collections import defaultdict
from difflib import get_close_matches as matcher
from difflib import SequenceMatcher
from scipy import sparse
import scipy.cluster.hierarchy as hclusterr
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.cluster import DBSCAN
from time import time

class CLUSTER:

    def __init__(self,fajl,epsvalue):
        self.version = '4.0'
        self.epsvalue = epsvalue
        self.infajl = fajl
        self.outfajl = fajl.replace('input','output').replace('.txt','_output_%s_%s.txt' % (self.epsvalue,self.version)).replace('post_','')
        self.outfajl_clean = fajl.replace('input','output').replace('.txt','_output_clean_%s_%s.txt' % (self.epsvalue,self.version)).replace('post_','')
        self.illegal_chars = re.compile(r'[^A-Z0-9 ]')
        self.upper_ascii = re.compile(r'[A-Z]')
        self.dseq = re.compile(r'^[0-9]+$')
        self.vocab = {}
        self.digit = re.compile(r'\d')
        self.remove_set = set()

    def _readin(self,fajl):
        self.original_lines = []
        with open(fajl,encoding='latin-1') as inf:
            for line in inf:
                #line2 = line.encode("utf-8")
                line2 = self.normalize_chars(line)
                line2 = self.preprocess(line2)
                if line2 == None:
                    continue
                else:
                    self.original_lines.append(line)
                    yield line2

    def normalize_chars(self,line):
        l1 = line
        line = ''.join(c for c in unicodedata.normalize('NFD',line) if unicodedata.category(c) != 'Mn')

        return line

    def preprocess(self,line):
        line = line.strip()
        ## Uppercase everything.
        line = line.upper()
        ## Remove country code.
        line = ','.join(line.split(',')[:-1])
        ## Remove punct marks, and rubbish chars.
        line = self.illegal_chars.sub(r' ',line)
        ## Remove digit sequenes.
        line = ' '.join([w for w in line.split() if self.digit_count(w)])
        #line = ' '.join([w for w in line.split() if not self.dseq.search(w)])
        ## Handle white spaces.
        line = ' '.join(line.split())

        ## Remove self.lines not containing alpha chars.
        if self.upper_ascii.search(line): 
            return line

    def digit_count(self,w):
        c = 0
        lw = len(w)
        for ch in w:
            if ch in '0123456789':
                c += 1
        if c == lw:
            return False
        elif lw < 4:
            return True
        ## If more than half of the chars are digits, remove word.
        elif (lw/2) <= c:
            return False
        else:
            return True

    def _give_ratio(self,wordcount):
        vocabcount = sum(self.vocab.values())
        print('Vocab count: %d' % vocabcount)
        ratio = int(wordcount) / vocabcount
        if ratio >= 0.0015:
            return True
        else:
            False

    def create_vocab(self):
        for line in self.lines:
            for w in line.split():
                try:
                    self.vocab[w] += 1
                except KeyError:
                    self.vocab[w] = 1

    def _create_frequent_words_set(self):
        self.create_vocab()
        frequency_list = sorted(self.vocab,key=lambda i: self.vocab[i],reverse=True)[:20]
        print(frequency_list)
        for w in frequency_list:
            if len(w) > 2:
                if self._give_ratio(self.vocab[w]):
                    print('Removing: %s, with count: %s' % (w,self.vocab[w]))
                    self.remove_set.add(w)

    def get_ratio(self,word2,word1):
        if word1 != word2:
            s = SequenceMatcher(None,word1,word2)
            #s = SequenceMatcher(lambda x: x == " ",
            ratio = s.real_quick_ratio()
            if ratio >= 0.9:
                #print(word1,word2)
                return word2
            else:
                return word1
        else:
            return word1

    def spell_check(self):
        self.create_vocab()
        self.new_lines = []
        spelldict = {}
        self.vocablist = [w for w in self.vocab.keys() if len(w) > 3 or not self.digit.search(w)]
        ## todo: sort vocablist

        c = 0
        for line in self.old_lines:
            c += 1
            for word in line.split():
                if len(word) < 6 or self.digit.search(word):
                    pass
                elif word in spelldict:
                    line = line.replace(word,spelldict[word])
                else:
                    matches = matcher(word,self.vocablist,2,0.9)
                    ## Retrieve words closer than distance.
                    if len(matches) > 1:
                        matched = matches[1]
                        if self.vocab[word] < self.vocab[matched]:
                            #print(word,matched)
                            line = line.replace(word,matched)
                            spelldict[word] = matched
            self.new_lines.append(line)

        self.lines = self.new_lines[:]

    def create_bigrams(self):
        cv = CountVectorizer()
        tv = TfidfVectorizer()
        bigram_lines = []
        for line in self.lines:
            ## Remove frequent words.
            line = ' '.join([w for w in line.split() if w not in self.remove_set])
            ## Remove spaces (optional).
            line = line.replace(' ','')
            line = '^'+''.join([c+c for c in line])+'$'
            line = ' '.join([line[i:i+2] for i in range(0,len(line),2)])
            bigram_lines.append(line)

        self.bigrams = tv.fit_transform(bigram_lines).toarray()
        print('Shape of input matrix: (%d,%d)' % (len(self.bigrams),len(self.bigrams[0])))

    def tfidf(self):
        self.vectorizer = TfidfVectorizer()
        self.doc_features = self.vectorizer.fit_transform(self.lines)
        self.terms = self.vectorizer.get_feature_names()
        #print(self.terms)
        #print(self.doc_features)
        print(self.doc_features.shape)

    def cosine(self,inmatrix):
        in_sparse = sparse.csr_matrix(inmatrix)
        self.cossim = cosine_similarity(in_sparse)
        self.distance = pairwise_distances(inmatrix,metric='cosine',n_jobs=-2)
        #self.cossim = cosine_similarity(in_sparse,dense_output=False)
        #('pairwise sparse output:\n{}\n'.format(self.cossim))
        print(self.cossim.shape)
        #print(self.cossim[1])
        #print(self.cossim[2])

    def create_clusters(self):
        pass

    def dbscan(self,inarray):
        s = 1
        mtrc = 'euclidean' # self.distance
        jobs = -2
        self.db = DBSCAN(eps=self.epsvalue,min_samples=s,metric=mtrc,n_jobs=jobs).fit_predict(inarray)
        #self.db = DBSCAN(eps=e,min_samples=s).fit_predict(self.doc_features.todense())

    def hcluster(self,inarray):
        threshold = 0.73
        self.hclusters = hclusterr.fclusterdata(inarray,threshold,criterion="distance")
        #self.hclusters = hclusterr.fclusterdata(self.doc_features.todense(), threshold, criterion="distance")
    
    def kmeans_clustering(self,inarray):
        clusters = 350
        self.km = KMeans(clusters).fit_predict(inarray)

    def map_clusters(self,clusters):
        self.mapped_clusters = {}
        self.mapped_clusters_clean = {}
        c = 0
        for i in clusters:
            try:
                self.mapped_clusters[i].append(self.original_lines[c])
                self.mapped_clusters_clean[i].append(self.old_lines[c])
            except KeyError:
                self.mapped_clusters[i] = [self.original_lines[c]]
                self.mapped_clusters_clean[i] = [self.old_lines[c]]
            c += 1

    def _write_out(self):
        with open(self.outfajl,'w',encoding='utf-8') as outf:
            ## Write original lines.
            for key in self.mapped_clusters.keys():
                outf.write('Cluster [%s]\n' % key)
                for v in self.mapped_clusters[key]:
                    outf.write('%s' % v)

        print('Number of output clusters in %s: %d' % (self.infajl,len(self.mapped_clusters.keys())))

    def process(self):
        ###############################
        ## Preprocessing
        self.lines = np.array(list(self._readin(self.infajl)))
        self.old_lines = self.lines[:]
        self.spell_check()
        #self.tfidf()
        if len(self.lines) > 10000:
            self._create_frequent_words_set()
        self.create_bigrams()
        ###############################
        ## Clustering
        #self.cosine(self.bigrams)
        #self.hcluster(self.doc_features.todense())
        #self.hcluster(self.bigrams)
        #self.cosine(self.bigrams)
        self.dbscan(self.bigrams)
        #self.dbscan(self.doc_features.todense())
        #self.kmeans_clustering(self.bigrams)
        ###############################
        ## Outputting
        #self.map_clusters(self.km)
        #self.map_clusters(self.hclusters)
        self.map_clusters(self.db)
        self._write_out()

if __name__ == '__main__':
    done = {'../input/249.txt'}
    with open('../1st_round_done',encoding='utf-8') as indone:
        for line in indone:
            line = line.rstrip()
            done.add(line)
    print(done)
    import glob
    import os
    for fn in glob.iglob('../input/*txt'):
        statfn = os.stat(fn)
        fnsize = statfn.st_size
        print('FN',fn,'|',fnsize)
        x = 40000
        ## For lines 0-1700.
        if 0 < fnsize < x:
            eps = 0.82
        ## For lines 1700-19590.
        if x < fnsize < 500000:
            eps = 0.72
        elif fnsize > 500000:
            eps = 0.62

        if fn not in done:
            print('Processing %s' % fn)
            t0 = time()
            ecuador = CLUSTER(fn,eps)
            ecuador.process()
            t1 = time()
            print('Time elapsed: %f sec' % (t1-t0))
