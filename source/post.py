# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import glob
import os
import sys

from cluster_lines import CLUSTER
from collections import OrderedDict
from time import time

def extract_clusters(version):
    d_all = {}
    for fn in glob.iglob('../output/*%s.txt' % version):
        with open(fn) as res:
            d = {}
            for line in res:
                if 'Cluster' in line and line[0] == 'C':
                    cl = line
                    d[cl] = []
                else:
                    d[cl].append(line)
        for cluster in d.keys():
            if len(d[cluster]) > 500:
                print(fn,cluster,len(d[cluster]))
                d_all['%s_%s' % (fn.replace('../output/','../post_input/'),cluster)] = d[cluster]

    return d_all

def write_post_input(d_all):
    for cluster in d_all.keys():
        outname = cluster.replace(' ','_').replace('\n','').replace(']','').replace('[','').replace('.txt','_txt') + '.txt'
        with open(outname,'w',encoding='utf-8') as outf:
            for line in d_all[cluster]:
                outf.write(line)

def merge_outputs(version):
    reclustered_names = set()
    for fn in glob.iglob('../output/*Cluster*%s.txt' % version):
        print('Cluster file',fn)
        fnl = fn.split('_txt')
        outname = fnl[0] + '_split_large_clusters.txt'
        outname = outname.replace('/output/','/post_output/')
        if outname in reclustered_names:
            origin_name = outname
            print('Already reclustered: %s' % origin_name)
        else:
            origin_name = fnl[0] + '.txt'
            print('new',origin_name)
        reclustered_names.add(outname)

        cl_part = fnl[1].split('output')[0]
        cluster_nr = 'Cluster [%s]\n' % cl_part.replace('Cluster','').replace('_','')
        ## Read in cluster file.
        with open(fn) as clfile:
            lclfile = []
            for line in clfile:
                lclfile.append(line)
        ## Read in country file.
        with open(origin_name) as origfile:
            dcl = OrderedDict()
            for line in origfile:
                if 'Cluster' in line and line[0] == 'C':
                    cl = line
                    dcl[cl] =[]
                else:
                    dcl[cl].append(line)
        ## Write reclustered data to country file.
        with open(outname,'w',encoding='utf-8') as outf:
            nr_current_cl = 0
            for clus in dcl.keys():
                if len(dcl[clus]) > 500 and origin_name != outname:
                    pass
                else:
                    outf.write('Cluster [%s]\n' % nr_current_cl)
                    nr_current_cl += 1
                    for ln in dcl[clus]:
                        outf.write(ln)
            for newline in lclfile:
                if 'Cluster' in newline and newline[0] == 'C':
                    outf.write('Cluster [%s]\n' % nr_current_cl)
                    nr_current_cl += 1
                else:
                    outf.write(newline)

if __name__ == '__main__':
    version = sys.argv[1]
    d_all = extract_clusters(version)
    write_post_input(d_all)
    for fn in glob.iglob('../post_input/*.txt'):
        print('Processing %s' % fn)
        t0 = time()
        eps = 0.72
        ecuador = CLUSTER(fn,eps)
        ecuador.process()
        t1 = time()
        print('Time elapsed: %f sec' % (t1-t0))
    merge_outputs(version)
